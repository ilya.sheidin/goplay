package main

func main() {
	cards := newDeck(false, "my_deck.txt")
	hand, remainingDeck := deal(cards, 5)
	hand.print()
	shuffledHand := hand.shuffle()
	remainingDeck = remainingDeck.shuffle()
	shuffledHand.print()
}

func newCard() string {
	return "Ace of spades"
}
