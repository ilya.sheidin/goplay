package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"strings"
	"time"
)

// Create a new type of deck
// which is a slice of strings
type deck []string

func newDeck(fromFile bool, fileName string) deck {
	// Load from file
	if fromFile {
		content, err := ioutil.ReadFile(fileName)
		if err != nil {
			return deck(strings.Split(string(content), "|"))
		}

	}

	cardSuits := []string{"Spades", "Hearts", "Diamonds", "Clubs"}
	cardValues := []string{"Ace", "Two", "Three", "Four"}
	listOfCards := deck{}
	for indCard := range cardSuits {
		for indVal := range cardValues {
			listOfCards = append(listOfCards, cardValues[indVal]+" of "+cardSuits[indCard])
		}
	}
	return listOfCards

}

func (d deck) print() {
	for index, value := range d {
		fmt.Println(index+1, value)
	}
}

func (d deck) shuffle() deck {
	for index, value := range d {
		// Replace cards
		source := rand.NewSource(time.Now().UnixNano())
		r := rand.New(source)
		randomNumber := r.Intn(len(d) - 1)
		if index != randomNumber {
			d[randomNumber] = d[index]
			d[index] = value
		}
	}
	return d
}

func (d deck) toString() string {
	return strings.Join(d, "|")
}

func deal(d deck, sliceNumber int) (deck, deck) {
	return d[:sliceNumber], d[sliceNumber:]
}

func (d deck) saveDeck(fileName string) error {
	return ioutil.WriteFile(fileName, []byte(d.toString()), 0644)
}
